# Debian
`setux.distros.debian`

[Setux] Debian support

[PyPI] - [Repo] - [Doc]


Debian [Distro] implementations

`setux.core.distro.Distro`




[PyPI]: https://pypi.org/project/setux_distros
[Repo]: https://framagit.org/louis-riviere-xyz/setux_distros
[Doc]: https://setux-distros.readthedocs.io/en/latest/debian
[Setux]: https://setux.readthedocs.io/en/latest

[Distro]: https://setux-core.readthedocs.io/en/latest/distro
