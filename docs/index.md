# Setux Distros

[Setux] OS support

[PyPI] - [Repo] - [Doc]


## Main [Distro] implementations
 * Debian
 * FreeBSD

note : [Setux Plus] implements other distros

## Installation

    $ pip install setux_distros


[PyPI]: https://pypi.org/project/setux_distros
[Repo]: https://framagit.org/louis-riviere-xyz/setux_distros
[Doc]: https://setux-distros.readthedocs.io/en/latest
[Setux]: https://setux.readthedocs.io/en/latest
[setux PLUS]: https://setux-plus.readthedocs.io/en/latest

[Distro]: https://setux-core.readthedocs.io/en/latest/distro
